/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import java.util.Date;

/**
 *
 * @author harshalneelkamal
 */
public class Customer extends Business.Abstract.User {

    private Date date;

    public Date getDate() {        
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
            
    public Customer(String password, String userName) {
        super(password, userName, "CUSTOMER");
    }

    @Override
    public boolean verify(String password) {
        return password.equals(super.getPassword());
    }
}
